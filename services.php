<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laundry and Dry Cleaning Services in Chennai, Bangalore & Pune</title>
    <meta name="description" content="Washnsave offers  the best laundry and dry cleaning services in Chennai and Bangalore cities. We also offer express delivery with cleaning of clothes in just 1 hour.">
    <meta name="keywords" content="laundry services in chennai, laundry service chennai, dry cleaners chennai, dry cleaning in chennai, dry cleaning chennai, laundry services in bangalore, laundry service in bangalore, laundry in bangalore, laundry service bangalore, laundry services in pune, laundry in pune, dry cleaners in pune, laundry service in pune, laundry service pune">
    <meta name="author" content="rudhisasmito.com">
    <meta name="google-site-verification" content="C3MHHIALf_Db6dTvkO3ClYrhCENS-_LvA6jic0b77P8" />

	<!-- ==============================================
	Favicons
	=============================================== -->
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">

	<!-- ==============================================
	CSS
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-image-gallery.min.css" />





	<!-- ==============================================
	Google Fonts
	=============================================== -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,700,900' rel='stylesheet' type='text/css'>


	<!-- Custom Stylesheet -->
	<link rel="stylesheet" type="text/css" href="css/style.css" />


    <script type="text/javascript" src="js/modernizr.min.js"></script>



</head>
<?php include('includes/header.php'); ?>
	<!-- BANNER -->
	<div class="section subbanner" style="background:url('images/slide_page.jpg') no-repeat center center;   -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="caption">
						<h3>SERVICES</h3>
						<ol class="breadcrumb">
						  <li><a href="index.php">Home</a></li>
						  <li class="active">Services</li>
						</ol>
					</div>
				</div>
			</div>
		</div>

	</div>


	<!-- ABOUT SECTION -->
	<!--<div id="services" class="section services">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">OUR SERVICES</h2>
						<p class="sublead">This template is designed with a unique and simple, so that it can promote and laundry business solution.</p>
					</div>
				</div>
			</div>

			<div class="row">

				<div class="services-item-full">
					<div class="col-xs-12 col-md-6">
						<div class="about-img">
							<img src="images/services-img-1.jpg" alt="" class="img-responsive" />
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="desc-wrap">
							<h4 class="title-page">COIN LAUNDRY</h4>
							<p>This template is designed with a unique and simple, so that it can promote and laundry business solution. This template can be an alternative for entrepreneurs engaged in the laundry. or could be an alternative for web developers who need to design websites laundry kategory business. As for the advantages of this template is multipage template which consists of a homepage, about page, services page, pricing page, faq page, blog and contact page. so that it can describe all the requirements needed for a business website.</p>
							<ul class="service-list">
								<li><i class="fa fa-check-circle"></i> Ready for all devices.</li>
								<li><i class="fa fa-check-circle"></i> Made with Adobe Muse.</li>
								<li><i class="fa fa-check-circle"></i> No Coding Required.</li>
								<li><i class="fa fa-check-circle"></i> Easy Costumizable.</li>
								<li><i class="fa fa-check-circle"></i> Affordable Price.</li>
							</ul>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="services-item-full">
					<div class="col-xs-12 col-md-6">
						<div class="desc-wrap">
							<h4 class="title-page">REDENTIAL LAUNDRY</h4>
							<p>This template is designed with a unique and simple, so that it can promote and laundry business solution. This template can be an alternative for entrepreneurs engaged in the laundry. or could be an alternative for web developers who need to design websites laundry kategory business. As for the advantages of this template is multipage template which consists of a homepage, about page, services page, pricing page, faq page, blog and contact page. so that it can describe all the requirements needed for a business website.</p>
							<ul class="service-list">
								<li><i class="fa fa-check-circle"></i> Ready for all devices.</li>
								<li><i class="fa fa-check-circle"></i> Made with Adobe Muse.</li>
								<li><i class="fa fa-check-circle"></i> No Coding Required.</li>
								<li><i class="fa fa-check-circle"></i> Easy Costumizable.</li>
								<li><i class="fa fa-check-circle"></i> Affordable Price.</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="about-img">
							<img src="images/services-img-2.jpg" alt="" class="img-responsive" />
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="services-item-full">
					<div class="col-xs-12 col-md-6">
						<div class="about-img">
							<img src="images/services-img-3.jpg" alt="" class="img-responsive" />
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="desc-wrap">
							<h4 class="title-page">BUSINESS LAUNDRY</h4>
							<p>This template is designed with a unique and simple, so that it can promote and laundry business solution. This template can be an alternative for entrepreneurs engaged in the laundry. or could be an alternative for web developers who need to design websites laundry kategory business. As for the advantages of this template is multipage template which consists of a homepage, about page, services page, pricing page, faq page, blog and contact page. so that it can describe all the requirements needed for a business website.</p>
							<ul class="service-list">
								<li><i class="fa fa-check-circle"></i> Ready for all devices.</li>
								<li><i class="fa fa-check-circle"></i> Made with Adobe Muse.</li>
								<li><i class="fa fa-check-circle"></i> No Coding Required.</li>
								<li><i class="fa fa-check-circle"></i> Easy Costumizable.</li>
								<li><i class="fa fa-check-circle"></i> Affordable Price.</li>
							</ul>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="services-item-full">
					<div class="col-xs-12 col-md-6">
						<div class="desc-wrap">
							<h4 class="title-page">INDUSTRY LAUNDRY</h4>
							<p>This template is designed with a unique and simple, so that it can promote and laundry business solution. This template can be an alternative for entrepreneurs engaged in the laundry. or could be an alternative for web developers who need to design websites laundry kategory business. As for the advantages of this template is multipage template which consists of a homepage, about page, services page, pricing page, faq page, blog and contact page. so that it can describe all the requirements needed for a business website.</p>
							<ul class="service-list">
								<li><i class="fa fa-check-circle"></i> Ready for all devices.</li>
								<li><i class="fa fa-check-circle"></i> Made with Adobe Muse.</li>
								<li><i class="fa fa-check-circle"></i> No Coding Required.</li>
								<li><i class="fa fa-check-circle"></i> Easy Costumizable.</li>
								<li><i class="fa fa-check-circle"></i> Affordable Price.</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="about-img">
							<img src="images/services-img-4.jpg" alt="" class="img-responsive" />
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="services-item-full">
					<div class="col-xs-12 col-md-6">
						<div class="about-img">
							<img src="images/services-img-5.jpg" alt="" class="img-responsive" />
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="desc-wrap">
							<h4 class="title-page">COSPLAY LAUNDRY</h4>
							<p>This template is designed with a unique and simple, so that it can promote and laundry business solution. This template can be an alternative for entrepreneurs engaged in the laundry. or could be an alternative for web developers who need to design websites laundry kategory business. As for the advantages of this template is multipage template which consists of a homepage, about page, services page, pricing page, faq page, blog and contact page. so that it can describe all the requirements needed for a business website.</p>
							<ul class="service-list">
								<li><i class="fa fa-check-circle"></i> Ready for all devices.</li>
								<li><i class="fa fa-check-circle"></i> Made with Adobe Muse.</li>
								<li><i class="fa fa-check-circle"></i> No Coding Required.</li>
								<li><i class="fa fa-check-circle"></i> Easy Costumizable.</li>
								<li><i class="fa fa-check-circle"></i> Affordable Price.</li>
							</ul>
						</div>
					</div>

					<div class="clearfix"></div>
				</div>

				<div class="services-item-full">
					<div class="col-xs-12 col-md-6">
						<div class="desc-wrap">
							<h4 class="title-page">SPACE LAUNDRY</h4>
							<p>This template is designed with a unique and simple, so that it can promote and laundry business solution. This template can be an alternative for entrepreneurs engaged in the laundry. or could be an alternative for web developers who need to design websites laundry kategory business. As for the advantages of this template is multipage template which consists of a homepage, about page, services page, pricing page, faq page, blog and contact page. so that it can describe all the requirements needed for a business website.</p>
							<ul class="service-list">
								<li><i class="fa fa-check-circle"></i> Ready for all devices.</li>
								<li><i class="fa fa-check-circle"></i> Made with Adobe Muse.</li>
								<li><i class="fa fa-check-circle"></i> No Coding Required.</li>
								<li><i class="fa fa-check-circle"></i> Easy Costumizable.</li>
								<li><i class="fa fa-check-circle"></i> Affordable Price.</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="about-img">
							<img src="images/services-img-6.jpg" alt="" class="img-responsive" />
						</div>
					</div>
					<div class="clearfix"></div>
				</div>



			</div>

		</div>
	</div>-->
	<div class="section about">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">Why Choose Wash And Save?</h2>
						<p class="sublead"></p>
					</div>
				</div>
			</div>

			<div class="row">

				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-flash"></div>
						</div>
						<div class="ket">
							<h4>OFFICIAL PARTNER WITH MAYTAG USA</h4>
							<p>Engineered to deliver dependable cleaning results with equally reliable savings.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-hand-o-up"></div>
						</div>
						<div class="ket">
							<h4>INDIVIDUAL WASHING</h4>
							<p>Hygienic and Safe that ensures no mixing of clothes or communal washing.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-recycle"></div>
						</div>
						<div class="ket">
							<h4>ECO-FRIENDLY</h4>
							<p>Energy Saving Schemes such as eco-friendly detergent, waste separation system and energy and water efficient machines.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-truck"></div>
						</div>
						<div class="ket">
							<h4>QUICK DELIVERY</h4>
							<p>We strive at completing washing and drying all your clothes within 48 hours, so you don’t have to spend all week with your dirty clothes. We also offer Express Delivery with clean clothes in 1 hour.</p>
						</div>
					</div>
				</div>

			</div>

      <div class="row">
			<div class="col-sm-12 col-md-12">
			<div class="page-title">
				<p>Do you spend lot of time separating darks and lights when doing laundry? Is it overwhelming to hold up clothing and debating which pile they should go into? You have found the right solution.</p>
 
				<p>A perfect experience for your wardrobe. Wash & Save is a Laundry Service with a difference. Wash & Save is every discerning person’s dream. Our attention to detail is precise.</p>
 
				<p>We operate brand new, high efficiency Maytag USA machines and your clothes come back neatly folded, nice and clean and smelling fresh.</p>

						<p>A perfect experience for your wardrobe. Wash and Save is a laundry service with a difference. Wash & Save is every discerning person’s dream. Our attention to detail is precise.</p>

						<p>Do you spend lot of time separating darks and lights when doing laundry? Is it overwhelming to hold up clothing and debating which pile they should go into?</p>

						<p>You have found the right solution.</p>

						<p>We operate brand new, high efficiency Maytag USA machines and your clothes come back neatly folded, nice and clean and smelling fresh.</p>
						<p>The services offered vary based on your locality.</p>
						<ul class="servicelist">
							<li>Laundry</li>
							<li>Wash &amp; Fold</li>
							<li>Dry Clean</li>
							<li>Steam Press</li>
							<li>Free Pick and Drop</li>
							<li>Starching</li>
							<li>Antiseptic Wash</li>
							<li>Fabric Softener</li>
							<li>Fabric Conditioner</li>
							<li>Shoe Servicing</li>
							<li>Carpet dry cleaning</li>
							<li>Shoe dry cleaning</li>
							<li>Sofa cover dry cleaning</li>
						</ul>

						<p>We pick up, wash and deliver within 48 hours</p>

						<p>Wash & Save: Fold - Machine Washed and Tumble Dried, Combined packaging</p>
						<p>Wash & Save: Iron - Machine Washed and Tumble Dried, Combined packaging</p>
						<p>Premium Laundry - Machine Washed and Hung Dried, Guaranteed cuff and collar cleaning, Individual packaging. </p>


						<p>Dry Clean - Petro-chemical Cleaning, Spot Removal of Stains, Crisp ironing, Individual Packaging</p>
				</div>
			</div>
			</div>

		</div>
	</div>

	<!-- FOOTER SECTION -->
<?php include('includes/footer.php'); ?>
