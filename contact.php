<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Wash & Save Enterprises - Chennai, Bangalore & Pune</title>
    <meta name="description" content="Contact Washnsave laundry and dry cleaning services in Chennai, Bangalore and Pune. They are the best laundry services in India.">
    <meta name="keywords" content="dry cleaners bangalore, best dry cleaners in bangalore, laundry franchise in bangalore, laundry business in bangalore, laundry services in chennai, laundry service chennai, dry cleaners chennai, dry cleaning in chennai, dry cleaners in pune, laundry service in pune, laundry service pune, best dry cleaners in pune">
    <meta name="author" content="rudhisasmito.com">
    <meta name="google-site-verification" content="C3MHHIALf_Db6dTvkO3ClYrhCENS-_LvA6jic0b77P8" />

	<!-- ==============================================
	Favicons
	=============================================== -->
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">

	<!-- ==============================================
	CSS
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-image-gallery.min.css" />





	<!-- ==============================================
	Google Fonts
	=============================================== -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,700,900' rel='stylesheet' type='text/css'>


	<!-- Custom Stylesheet -->
	<link rel="stylesheet" type="text/css" href="css/style.css" />


    <script type="text/javascript" src="js/modernizr.min.js"></script>



</head>
<?php include('includes/header.php'); ?>
<script type="text/javascript" src="../pariharams/admin/js/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
	function valdateContact(frm){
		var result = true;
		if(result) result = validateRequired(frm.txtName, 'Please enter your name');
		if(result) result = validateRequired(frm.txtEmail, 'Please enter your email');
		if(result) result = validateEmail(frm.txtEmail, 'Invalid email id');
		if(result) result = validateRequired(frm.txtPhone, 'Please enter your phone number');
		// if(result) result = validateRequired(frm.txtAddress, 'Please enter your address');
		// if(result) result = validateRequired(frm.txtCity, 'Please enter your city');
		// if(result) result = validateRequired(frm.txtZip, 'Please enter your zip code');
		// if(result) result = validateRequired(frm.txtMessage, 'Please enter your message');
		return result;
	}

</script>


	<!-- BANNER -->
	<div class="section subbanner" style="background:url('images/slide_page.jpg') no-repeat center center;   -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="caption">
						<h3>CONTACT</h3>
						<ol class="breadcrumb">
						  <li><a href="index.php">Home</a></li>
						  <li class="active">Contact</li>
						</ol>
					</div>
				</div>
			</div>
		</div>

	</div>


	<!-- ABOUT SECTION -->
	<div id="services" class="section services">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">CONTACT US</h2>
						<!--<p class="sublead">This template is designed with a unique and simple, so that it can promote and laundry business solution.</p>-->
					</div>
				</div>
			</div>


			<div class="row pbot-main">

				<div class="col-sm-12 col-md-6">

                     <?php
		 	  $strMsg ="";
			  if(isset($_POST['contactVal'])){


                $sendmail = 'info@washnsave.in';	// to email

				$subject = 'Washnsave! - Contact Us Information!'; //subject

				//message
				 $message='<table border="0" align="left" cellspacing="0" cellpadding="10">

				<tr>
				<td>Dear Admin,<br /><br />
				<strong>Name :</strong> '.$_REQUEST['txtName'].'<br /><br />
				<strong>Email :</strong> '.$_REQUEST['txtEmail'].'<br /><br />
				<strong>Phone Number :</strong> '.$_REQUEST['txtPhone'].'<br /><br />
				<strong>Address :</strong> '.$_REQUEST['txtAddress'].'<br /><br />
				<strong>City :</strong> '.$_REQUEST['txtCity'].'<br /><br />
				<strong>Zip :</strong> '.addslashes($_REQUEST['txtZip']).'<br /><br />
				<strong>Message :</strong> '.addslashes($_REQUEST['txtMessage']).'<br /><br />



				</td>
				</tr>
				<tr>
				<td>'.nl2br('Regards <br/> Washnsave <br/> http://washnsave.in/').'</td>
				</tr>
				</table>';

				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= "From: Washnsave <info@washnsave.in> \r\n";
				$res = @mail($sendmail, $subject, $message, $headers);
				 if($res){
                    $strMsg = 'The details have been sent successfully. We will get back to you soon!';
                }

               // echo '<div class="alert alert-success" style="font-weight:bold;">The details have been sent successfully. We will get back to you soon!</div>';


    }
    ?>

    	<?php if($strMsg !=''){ echo '<div class="alert alert-success" style="font-weight:bold;color:green;">'.$strMsg.'</div>'; }?>

					<form action="contact.php" method="post" name="frmContact" onsubmit="return valdateContact(this)">
						<div class="form-group">
							<input type="text" class="form-control" id="txtName" name="txtName" placeholder="Enter Name *" required>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="txtEmail" name="txtEmail" placeholder="Enter Email *" required>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="txtPhone" name="txtPhone" placeholder="Enter Phone Number *" required>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<textarea id="txtAddress"  class="form-control" rows="3" name="txtAddress" placeholder="Enter Address"></textarea>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="txtCity" name="txtCity" placeholder="Enter City">
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="txtZip" name="txtZip" placeholder="Enter Zip">
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							 <textarea id="txtMessage" class="form-control" rows="5" name="txtMessage" placeholder="Enter Your Message"></textarea>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<div id="success"></div>
                             <input type="hidden" name="contactVal" value="yes" />
							<button type="submit" class="btn btn-default">SEND A MESSAGE</button>
						</div>
					</form>


				</div>

				<div class="col-sm-12 col-md-6">

					<div class="maps-wraper">
						<div id="cd-zoom-in"></div>
						<div id="cd-zoom-out"></div>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3886.0591863373616!2d80.1732873531992!3d13.095435532906746!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a5263ee864f65e5%3A0x98ef2048e8c28040!2sNate+Commercial+Laundry+Pvt+Ltd!5e0!3m2!1sen!2sin!4v1455102396764" width="600" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>


					<div class="row contact-address">
						<div class="col-sm-12 col-md-6">
							<div class="contact-address-item">
								<div class="contact-address-heading">ADDRESS </div>
                                <div class="desc-1">CHENNAI</div>
								<p>
								WASH & SAVE ENTERPRISES <br />
								D20, 3rd Street,  <br />
                                Ambattur Industrial Estate, <br />
								Ambattur, Chennai - 600 058.<br />
                                Email: <a href="mailto: Email: info@natecle.in">info@natecle.in  </a><br/>
								Phone : +91 - 44 - 32001617 <br/>
                                Ravichandiran:  +91 98847 55427 <br/>
                                Prashant        +91 94455 61308
								</p>

                                <div class="desc">
                                        <div class="desc-1">BANGALORE</div>
                                        <div class="desc-3">
                                      WASH & SAVE ENTERPRISES <br/>
     No.15, Krishnappa Layout, Kerekodi, <br/> Hosakerahalli BSK 3rd Stage, <br/>  Bangalore 560085	.<br/>  Email: <a href="mailto: Email: info@natecle.in">info@natecle.in </a> <br/></div>
                                        </div>
                                <div class="desc-4">
                                              Manjula         +91 76766 53014 <br/>
                                              Satyanarayana   +91 99869 26066
                                        </div> <br/>

                                <div class="footer-getintouch-item">
                                    <div class="desc">
                                        <div class="desc-1">PUNE</div>

                                        <div class="desc-3">
                                      WASH & SAVE ENTERPRISES <br/>
     Gate no – 1, S.no – 40, Chimbli Phata, <br/>   Nasik Road, Kuruli, <br/> Tal-Khed Dist, <br/>Pune – 410501. <br/>  Email: <a href="mailto: Email: info@natecle.in">info@natecle.in </a> <br/></div>
                                        </div>
                                        <div class="desc-4">
                                              Kuppuraj J       +91 98843 20666   <br/>
                                              Gopikrishnan   +91 98419 19191
                                        </div>
                                </div>
							</div>
						</div>
						<!--<div class="col-sm-12 col-md-6">
							<div class="contact-address-item">
								<div class="contact-address-heading">ADDRESS 2</div>
								<p>
								45 Embunpagi Street, Pekanbaru<br />
								40021 Riau. <br />
								Indonesia.<br /><br />
								Phone : +62 7144 3300
								</p>
							</div>
						</div>-->
						<!--<div class="col-sm-12 col-md-6">
							<div class="contact-address-item">
								<div class="contact-address-heading">OPENING HOURS</div>
								<p>
								Monday - Friday : 09:00 - 20:00 pm<br />
								Saturday : 09:00 - 17:00
								</p>
							</div>
						</div>-->

					</div>


				</div>


			</div>



		</div>
	</div>


	<!-- FOOTER SECTION -->
<?php include('includes/footer.php'); ?>
