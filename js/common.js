// Validate Required
function validateRequired(field, msg, min, max){
	var test = "pass";
	if(field.value.length == 0) {
		test = "fail";
	}else if(min && field.value.length < min) {
		msg = msg + "\nMin Length should be " + min;
		test = "fail";
	}else if(max && field.value.length > max) {
		msg = msg + "\nMax Length should be " + max;
		test = "fail";
	}
	
	if(test == "fail"){
		if (msg) alert(msg);
		field.focus();
		field.select();
		return false;
	}
	return true;
}

// Validate Extension
function CheckExtension(field, msg){
	
	var ext = field.value.split(".")
	if(ext[1] == 'doc'){
		return true;
	}
	else {
		if (msg != ''){
			alert(msg);
		}
		field.focus();
		return false;
	}

}

//Validate Extension
function CheckImgExtension(field, msg){
	
	var ext = field.value.split(".")
	if((ext[1] == 'jpg') || (ext[1] == 'jpeg') || (ext[1] == 'gif')){
		return true;
	}
	else {
		if (msg != ''){
			alert(msg);
		}
		field.focus();
		return false;
	}

}


// Validate word count
function count_words(field, msg, min, max)
{
    var test = "pass";
	var no_words = field.value.split(" ");
	
	if(field.value.length == 0) {
		test = "fail";
	}else if(min && no_words.length < min) {
		msg = msg + "\nMin Word should be " + min;
		test = "fail";
	}else if(max && no_words.length > max) {
		msg = msg + "\nMax Word should be " + max;
		test = "fail";
	}
	
	if(test == "fail"){
		if (msg) alert(msg);
		field.focus();
		field.select();
		return false;
	}
	return true;
}

// Validate Number
function validateNumber(field, msg, min, max){
	if (!min) { min = 0 }
	if (!max) { max = 255 }

	if ( (parseInt(field.value) != field.value) ||
             field.value.length < min ||
             field.value.length > max) {
		alert(msg);
		field.focus();
		field.select();
		return false;
	}

	return true;
}
//Validate mobileNumber
function validateMobileNumber(field, msg, min, max){
	if (!min) { min = 0 }
	if (!max) { max = 12 }

	if ( (parseInt(field.value) != field.value) ||
             field.value.length < min ||
             field.value.length > max) {
		alert(msg);
		field.focus();
		field.select();
		return false;
	}

	return true;
}

// Validate Mail IDs
function validateEmail(field, msg){
	if (!field.value) {
		return true;
	}

	var re_mail = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (!re_mail.test(field.value)) {
		if (msg != '') { alert(msg); }
		field.focus();
		field.select();
		return false;
	}

	return true;
}

// Validate Url
function validateUrl(field, msg){
	if (!field.value) {
		return true;
	}

	var re_url = /^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/;
	if (!re_url.test(field.value)) {
		if (msg != '') { alert(msg); }
		field.focus();
		field.select();
		return false;
	}

	return true;
}

// Validate Alpha-Numeric
function validateAlphaNumeric(field, msg){
	var numaric = field.value;
	for(var j=0; j<numaric.length; j++) {
		var alphaa = numaric.charAt(j);
		var hh = alphaa.charCodeAt(0);
		
		if(!(hh > 47 && hh<59) || (hh > 64 && hh<91) || (hh > 96 && hh<123)){
			if (msg != ''){
				alert(msg);
			}
			field.focus();
			field.select();
			return false;
		}
	}
	return true;
}

// Validate Combo
function validateCombo(field, msg){
	
	if(field.selectedIndex == 0){
		if (msg != ''){
			alert(msg);
		}
		field.focus();
		return false;
	}

	return true;
}

// Validate Checkbox
function validateCheckbox(field, msg){
	
	if(field.checked == false){
		if (msg != ''){
			alert(msg);
		}
		field.focus();
		return false;
	}

	return true;
}

// Validate at least one textbox
function validateAtleastOne(field1, field2, field3, field4, field5, field6, msg){
	
	if((field1.value == "") && (field2.value == "") && (field3.value == "") && (field4.value == "") && (field5.value == "") && (field6.value == "")) {
		if (msg != ''){
			alert(msg);
		}
		//field1.focus();
		return false;
	}

	return true;
}

// Validate at least one Combo
function validateAtleastOneCombo(field1, field2, msg){
	
	if((field1.selectedIndex == 0) && (field2.selectedIndex == 0)) {
		if (msg != ''){
			alert(msg);
		}
		field1.focus();
		return false;
	}

	return true;
}


function ValidateAnyOne(field1, field2, msg, msg1) {
	if(field1.value || field2.value) {
		if (field1.value != ''){
			if(parseInt(field1.value) != field1.value) {
				alert(msg);
				return false;
			}
			
		}
		return true	;
	}
	alert(msg1);
	return false;
}

function compareFields(field1, field2, msg) {
	if(field1.value != field2.value) {
		if (msg != ''){
			alert(msg);
		}
		return false;
	}

	return true;
}
function compareFields2(field1, field2, msg) {
	if(field1.value == field2.value) {
		if (msg != ''){
			alert(msg);
		}
		return false;
	}

	return true;
}

// to Radio button
function ValidateRadio(field, msg) {
	for(i=0; i<field.length; i++)
	{
		if(field[i].checked) {
			return true;
		}
	}
	if (msg != ''){
			alert(msg);	
		}
		return false;	
}

function validatePhoneno(field,msg) {
		if((field.value==null) ||(field.value=="")) {
			alert('Please enter your phone number');
			field.focus();
			return false;
		}
		/*//else if(field.value.search(/^[0-9]+$/ == -1) {*/
		else if((field.value.search(/\d{3}\-\d{8}/) ==-1) || (field.value.search(/\d{3}\ \d{8}/) ==-1)) {
			alert("Phone Number Should be xxx-xxxxxxxx or xxx xxxxxxxx");
			fiels.focus();
			return false;
		}
}

function showDiv(vDivId) {
	document.getElementById(vDivId).style.display = 'block';
}

function hideDiv(vDivId) {
	document.getElementById(vDivId).style.display = 'none';
}


function checkAll() {
   var items = document.frmResult.elements.length;
   for (i = 0; i < items; i++)
	if(document.frmResult.elements[i].type == 'checkbox'){
		document.frmResult.elements[i].checked = true;
	}
}

function uncheckAll() {
   var items = document.frmResult.elements.length;
	   for (i = 0; i < items; i++)
		if(document.frmResult.elements[i].type == 'checkbox'){
		document.frmResult.elements[i].checked = false;
	}
   }

function trueCheck(field, msg){
	if(field.value == 'true'){
		return true;
	}else{
		alert(msg);
		field.focus();
		return false;
	}	
}
 
function numbersonly(myfield, e, dec) {
  var key;
  var keychar;

  if (window.event)
    key = window.event.keyCode;
  else if (e)
    key = e.which;
  else
    return true;
  keychar = String.fromCharCode(key);

  // control keys
  if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;

  // numbers
  else if ((("+0123456789").indexOf(keychar) > -1))
    return true;

  // decimal point jump
  else if (dec && (keychar == ".")) {
    myfield.form.elements[dec].focus();
    return false;
  } else
    return false;
}

function ConfirmMsg(strMsg) {
	if(strMsg==1) {
		strString = "modify"; }
		else if(strMsg==2) {
		strString = "delete"; }
	
	con = confirm("Do you want to "+ strString +" this record?");
	if(con==true) {
		return true;
	}
	else {
		return false;
	}
}

function checkField(Form)
{
	if((Form.selCountry) && (Form.selCountry[0]))
	{
		if(Form.selCountry[0].selected)
		{
				alert("Please select the valid country.")
				Form.selCountry.focus();
				return true;
		}
		else
		return false;
	}
	else
		return false;
}


function checkRadio (frmName, rbGroupName) {
 var radios = document[frmName].elements[rbGroupName];
 for (var i=0; i < radios.length; i++) {
  if (radios[i].checked) {
   return true;
  }
 }
 return false;
} 


function trim(str){
return str.replace(/^\s+|\s+$/g,''); }

function hide_msg() {
	$("#divMsg").html("");
}


$(function(){
		   
	var offset = 500;
	var duration = 1000;
	$(window).scroll(function() {
		if ($(this).scrollTop() > offset) {
			$('#scroll-to-top').fadeIn(duration);
		} else {
			$('#scroll-to-top').fadeOut(duration);
		}
	});
	
	$('#scroll-to-top').click(function(event) {
		event.preventDefault();
		$('html, body').animate({scrollTop: 0}, duration);
		return false;
	});
});

function redirect(aid){
	$.get("adclicks.php?aid="+aid, function(data) {
		//window.open(url, '_blank');
		return false;
	});
}

/*
	Totem Ticker Plugin
	Copyright (c) 2011 Zach Dunn / www.buildinternet.com
	Released under MIT License
	--------------------------
	Structure based on Doug Neiner's jQuery plugin blueprint: http://starter.pixelgraphics.us/
*/
(function( $ ){
	
	if(!$.omr){
		$.omr = new Object();
	};

	$.omr.totemticker = function(el, options ) {
	  	
	  	var base = this;
	  	
		//Define the DOM elements
	  	base.el = el;
	  	base.$el = $(el);
	  	
	  	// Add a reverse reference to the DOM object
        base.$el.data("omr.totemticker", base);
	  	
	  	base.init = function(){
            base.options = $.extend({},$.omr.totemticker.defaultOptions, options);
            
            //Define the ticker object
           	base.ticker;
			
			//Adjust the height of ticker if specified
			base.format_ticker();
			
			//Setup navigation links (if specified)
			base.setup_nav();
			
			//Start the ticker
			base.start_interval();
			
			//Debugging info in console
			//base.debug_info();
        };
		
		base.start_interval = function(){
			
			//Clear out any existing interval
			clearInterval(base.ticker);
			
	    	base.ticker = setInterval(function() {
	    	
	    		base.$el.find('li:first').animate({
	            	marginTop: '-' + base.options.row_height,
	            }, base.options.speed, function() {
	                $(this).detach().css('marginTop', '0').appendTo(base.$el);
	            });
	            
	    	}, base.options.interval);
	    }
	    
	    base.reset_interval = function(){
	    	clearInterval(base.ticker);
	    	base.start_interval();
	    }
	    
	    base.stop_interval = function(){
	    	clearInterval(base.ticker);
	    }
	
		base.format_ticker = function(){
		
			if(typeof(base.options.max_items) != "undefined" && base.options.max_items != null) {
				
				//Remove units of measurement (Should expand to cover EM and % later)
				var stripped_height = base.options.row_height.replace(/px/i, '');
				var ticker_height = stripped_height * base.options.max_items;
			
				base.$el.css({
					height		: ticker_height + 'px', 
					overflow	: 'hidden',	
				});
				
			}else{
				//No heights were specified, so just doublecheck overflow = hidden
				base.$el.css({
					overflow	: 'hidden',
				})
			}
			
		}
	
		base.setup_nav = function(){
			
			//Stop Button
			if (typeof(base.options.stop) != "undefined"  && base.options.stop != null){
				$(base.options.stop).click(function(){
					base.stop_interval();
					return false;
				});
			}
			
			//Start Button
			if (typeof(base.options.start) != "undefined"  && base.options.start != null){
				$(base.options.start).click(function(){
					base.start_interval();
					return false;
				});
			}
			
			//Previous Button
			if (typeof(base.options.previous) != "undefined"  && base.options.previous != null){
				$(base.options.previous).click(function(){
					base.$el.find('li:last').detach().prependTo(base.$el).css('marginTop', '-' + base.options.row_height);
					base.$el.find('li:first').animate({
				        marginTop: '0px',
				    }, base.options.speed, function () {
				        base.reset_interval();
				    });
				    return false;
				});
			}
			
			//Next Button
			if (typeof(base.options.next) != "undefined" && base.options.next != null){
				$(base.options.next).click(function(){
					base.$el.find('li:first').animate({
						marginTop: '-' + base.options.row_height,
			        }, base.options.speed, function() {
			            $(this).detach().css('marginTop', '0px').appendTo(base.$el);
			            base.reset_interval();
			        });
			        return false;
				});
			}
			
			//Stop on mouse hover
			if (typeof(base.options.mousestop) != "undefined" && base.options.mousestop === true) {
				base.$el.mouseenter(function(){
					base.stop_interval();
				}).mouseleave(function(){
					base.start_interval();
				});
			}
			
			/*
				TO DO List
				----------------
				Add a continuous scrolling mode
			*/
			
		}
		
		base.debug_info = function()
		{
			//Dump options into console
			console.log(base.options);
		}
		
		//Make it go!
		base.init();
  };
  
  $.omr.totemticker.defaultOptions = {
  		message		:	'Ticker Loaded',	/* Disregard */
  		next		:	null,		/* ID of next button or link */
  		previous	:	null,		/* ID of previous button or link */
  		stop		:	null,		/* ID of stop button or link */
  		start		:	null,		/* ID of start button or link */
  		row_height	:	'100px',	/* Height of each ticker row in PX. Should be uniform. */
  		speed		:	800,		/* Speed of transition animation in milliseconds */
  		interval	:	4000,		/* Time between change in milliseconds */
		max_items	: 	null, 		/* Integer for how many items to display at once. Resizes height accordingly (OPTIONAL) */
  };
  
  $.fn.totemticker = function( options ){
    return this.each(function(){
    	(new $.omr.totemticker(this, options));
  	});
  };
  
})( jQuery );