<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laundry Franchise | Laundry Business Plan | Franchise for Sale</title>
    <meta name="description" content="If you are an entrepreneur looking for a business plan opportunities in the Laundry sector, then a Wash & Save franchise will be the perfect platform to achieve your business goals. We offer you the best laundry equipment and guide you to be a pioneer in the market">
    <meta name="keywords" content="laundry business, laundry franchise, laundry business plan, laundry franchise opportunities, franchise laundry business, laundry best franchise, laundry franchise for sale, franchise for laundry business, maytag commercial laundry franchise, laundry business for sale">
    <meta name="author" content="rudhisasmito.com">
    <meta name="google-site-verification" content="C3MHHIALf_Db6dTvkO3ClYrhCENS-_LvA6jic0b77P8" />

	<!-- ==============================================
	Favicons
	=============================================== -->
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">

	<!-- ==============================================
	CSS
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-image-gallery.min.css" />





	<!-- ==============================================
	Google Fonts
	=============================================== -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,700,900' rel='stylesheet' type='text/css'>


	<!-- Custom Stylesheet -->
	<link rel="stylesheet" type="text/css" href="css/style.css" />


    <script type="text/javascript" src="js/modernizr.min.js"></script>



</head>
<?php include('includes/header.php'); ?>
<script type="text/javascript" src="../pariharams/admin/js/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
	function valdateContact(frm){
		var result = true;
		if(result) result = validateRequired(frm.txtName, 'Please enter your name');
		if(result) result = validateRequired(frm.txtEmail, 'Please enter your email');
		if(result) result = validateEmail(frm.txtEmail, 'Invalid email id');
		if(result) result = validateRequired(frm.txtPhone, 'Please enter your phone number');
		return result;
	}

</script>
	<!-- BANNER -->
	<div class="section subbanner" style="background:url('images/slide_page.jpg') no-repeat center center;   -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="caption">
						<h3>FRANCHISEE</h3>
						<ol class="breadcrumb">
						  <li><a href="index.php">Home</a></li>
						  <li class="active">Franchisee</li>
						</ol>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!--<div class="section about">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">Why Choose Wash And Save?</h2>
						<p class="sublead"></p>
					</div>
				</div>
			</div>

			<div class="row">

				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-flash"></div>
						</div>
						<div class="ket">
							<h4>OFFICIAL PARTNER WITH MAYTAG</h4>
							<p>Engineered to deliver dependable cleaning results with equally reliable savings.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-money"></div>
						</div>
						<div class="ket">
							<h4>INDIVIDUAL WASHING</h4>
							<p>Hygienic and Safe that ensures no mixing of clothes or communal washing.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-truck"></div>
						</div>
						<div class="ket">
							<h4>Eco-FRIENDLY</h4>
							<p>Energy Saving Schemes such as eco-friendly detergent, waste separation system and energy and water efficient machines.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-life-bouy"></div>
						</div>
						<div class="ket">
							<h4>1 HOUR DELIVERY</h4>
							<p>We strive at completing washing and drying all your clothes within 1 hour, so you don’t have to spend half a day with your dirty clothes.</p>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>-->
    <div class="section services">
		<div class="container">

        	<div class="row">
            	<div class="col-sm-12 col-md-12">
                	<div class="page-title" style="margin-bottom:0px;">
						<h2 class="lead">THE FRANCHISE OPPORTUNITY</h2>
                       <div class="page-left">
                        <!-- <p>If you are an entrepreneur looking for business opportunities in the Laundry sector, then a <span style="font-weight:bold;">Wash & Save </span> franchise will be the perfect platform to achieve your business goals. Laundry is a necessary evil. The laundry industry has weathered many a weak economic times. Washing and drying clothes is a basic human need and a simple business. Franchising offers a huge opportunity to be your own boss minus the risks associated with starting a business from scratch.</p>
                        <p>The <span style="font-weight:bold;">Wash & Save</span> team has the experience and knowledge to get you up and running quickly and to help you avoid the steep learning curve that comes with starting a new business. Whether you have previous laundry experience or are new to the industry, our trained staff will take you through every aspect of opening and operating your laundry efficiently and effectively.</p>
                        <p>Furthermore, as a <span style="font-weight:bold;">Wash & Save </span> franchisee you are in business for yourself, but not by yourself. You have the rewards of being an independent business owner, while at the same time receiving the experience of the <span style="font-weight:bold;">Wash & Save</span> team to back you up.</p>
                        <p>We would love to hear from you to tell you more.</p>
                        <p>Excited? Get in touch with us now!</p> -->
                        <p>Let's talk Commercial Laundry!</p>

                        <p>If you are an entrepreneur looking for business opportunities in the Laundry sector, then a <span style="font-weight:bold;">Wash & Save </span> franchise will be the perfect platform to achieve your business goals. Laundry is a necessary evil. The laundry industry has weathered many a weak economic times. Washing and drying clothes is a basic human need and a simple business. Franchising offers a huge opportunity to be your own boss minus the risks associated with starting a business from scratch.</p>

                        <p>The <span style="font-weight:bold;">Wash & Save</span> team has the experience and knowledge to get you up and running quickly and to help you avoid the steep learning curve that comes with starting a new business. Whether you have previous laundry experience or are new to the industry, our trained staff will take you through every aspect of opening and operating your laundry efficiently and effectively.</p>

                        <p class="lead" style="font-size: 22px;">Why Franchise with Wash & Save?</p>

                        <p>Just when you thought laundry services couldn&#39;t have gotten easier, Wash and Save introduces itself in a very unique and dynamic role in the laundry industry.</p>

                        <p>Wash &amp; Save offers individual washing of your clothes, in a hygienic way, and offer quick delivery options.</p>

                        <p>Wash &amp; Save employs energy saving methods like eco-friendly detergents and water efficient devices.</p>

                        <p>Applicable for all, experienced or novices to the laundry service, the Wash &amp; Save team guides new partners through the process of setting up their own laundry business.</p>

                        <p>World class facility</p>

                        <p>Top notch imported equipment</p>

                        <p>Door to door pick-up and delivery</p>

                        <p>Wash and fold services</p>

                        <p>Press</p>
                       </div>
					</div>

                </div>
            </div>

			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">FRANCHISEE PARTNERS</h2>
						<p class="sublead">The Easiest and Cleanest Investment is here for you!.</p>
					</div>
				</div>
			</div>

			<div class="row">

				<div class="col-sm-6 col-md-4">
					<div class="services-item left">
						<div class="icon">
							<img src="images/home-service-img-1.jpg" alt="" class="img-circle" />
						</div>
						<div class="ket">
							<h4></h4>
							<p>Everyone needs Clean Clothes and It’s a necessity in Life.</p>
						</div>
					</div>
					<div class="services-item left">
						<div class="icon">
							<img src="images/home-service-img-2.jpg" alt="" class="img-circle" />
						</div>
						<div class="ket">
							<h4></h4>
							<p>Few or Almost No Employees needed.</p>
						</div>
					</div>
					<div class="services-item left">
						<div class="icon">
							<img src="images/home-service-img-3.jpg" alt="" class="img-circle" />
						</div>
						<div class="ket">
							<h4></h4>
							<p>Provides a Consistent Source of Revenue.</p>
						</div>
					</div>

				</div>

				<div class="col-sm-6 col-md-4 col-md-offset-4">
					<div class="services-item right">
						<div class="icon">
							<img src="images/home-service-img-4.jpg" alt="" class="img-circle" />
						</div>
						<div class="ket">
							<h4></h4>
							<p>Cash Business that pay in advance with little labor required.</p>
						</div>
					</div>
					<div class="services-item right">
						<div class="icon">
							<img src="images/home-service-img-5.jpg" alt="" class="img-circle" />
						</div>
						<div class="ket">
							<h4></h4>
							<p>Recession Proof Business Investment.</p>
						</div>
					</div>
					<div class="services-item right">
						<div class="icon">
							<img src="images/home-service-img-6.jpg" alt="" class="img-circle" />
						</div>
						<div class="ket">
							<h4></h4>
							<p>Enjoy whole Year Business with NO Seasonal Fluctuation.</p>
						</div>
					</div>

				</div>

				<div class="col-sm-12 col-md-4 col-md-offset-4">
					<div class="services-item-image">
						<img src="images/service_img_home2-u24208-fr.png" alt="" />
					</div>
				</div>

			</div>

      <div class="row">
      <div class="col-sm-12 col-md-12">
      <div class="page-title" style="margin: 0;">
        <p>Furthermore, as a <span style="font-weight:bold;">Wash & Save </span> franchisee you are in business for yourself, but not by yourself. You have the rewards of being an independent business owner, while at the same time receiving the experience of the <span style="font-weight:bold;">Wash & Save</span> team to back you up.</p>
        <p>We would love to hear from you to tell you more.</p>
        <p>Excited? Get in touch with us now!</p>
        </div>
      </div>
    </div>

		</div>
	</div>

    <div id="services" class="section services">
		<div class="container">
			<!--  -->
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">CONTACT US</h2>
					</div>
				</div>
			</div>


			<div class="row pbot-main">

				<div class="col-md-6 col-md-offset-3">

                    <?php
          $strMsg ="";
          if(isset($_POST['contactVal'])){


               $sendmail = 'info@washnsave.in';	// to email

          $subject = 'Washnsave! - Franchise Contact Us Information!'; //subject

          //message
          $message='<table border="0" align="left" cellspacing="0" cellpadding="10">

          <tr>
          <td>Dear Admin,<br /><br />
          <strong>Name :</strong> '.$_REQUEST['txtName'].'<br /><br />
          <strong>Email :</strong> '.$_REQUEST['txtEmail'].'<br /><br />
          <strong>Phone Number :</strong> '.$_REQUEST['txtPhone'].'<br /><br />
          </td>
          </tr>
          <tr>
          <td>'.nl2br('Regards <br/> Washnsave <br/> http://washnsave.in/').'</td>
          </tr>
          </table>';

          $headers  = 'MIME-Version: 1.0' . "\r\n";
          $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
          $headers .= "From: Washnsave <info@washnsave.in> \r\n";
          $res = @mail($sendmail, $subject, $message, $headers);
          if($res){
                   $strMsg = 'The details have been sent successfully. We will get back to you soon!';
               }

              // echo '<div class="alert alert-success" style="font-weight:bold;">The details have been sent successfully. We will get back to you soon!</div>';


          }
          ?>

          <?php if($strMsg !=''){ echo '<div class="alert alert-success" style="font-weight:bold;color:green;">'.$strMsg.'</div>'; }?>

          <!-- <form action="franchisee.php" method="post" name="frmContact" onsubmit="return valdateContact(this)"> -->
          <form action="franchisee.php" method="post" name="frmContact" class="form-contact shake" onsubmit="return valdateContact(this)">
						<div class="form-group">
							<input type="text" class="form-control" id="txtName" name="txtName" placeholder="Enter Name *" required>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="txtEmail" name="txtEmail" placeholder="Enter Email *" required>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="txtPhone" name="txtPhone" placeholder="Enter Phone Number *" required>
							<div class="help-block with-errors"></div>
						</div>
						<!--<div class="form-group">
							<input type="text" class="form-control" id="address" placeholder="Enter Address">
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="city" placeholder="Enter City">
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="zip" placeholder="Enter Zip">
							<div class="help-block with-errors"></div>
						</div>
            <div class="form-group">
							 <textarea id="message" class="form-control" rows="5" placeholder="Enter Your Message *" required></textarea>
							<div class="help-block with-errors"></div>
						</div>-->
						<div class="form-group">
							<div id="success"></div>
               <input type="hidden" name="contactVal" value="yes" />
							<button type="submit" class="btn btn-default">SEND A MESSAGE</button>
						</div>
					</form>


				</div>


			</div>



		</div>
	</div>

	<!-- FOOTER SECTION -->
<?php include('includes/footer.php'); ?>
