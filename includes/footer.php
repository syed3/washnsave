
	<div class="footer">

		<div class="f-desc">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="footer-item">
							<div class="footer-logo">
								<img src="images/logo.png" alt="" />
							</div>
                            <div class="footer-getintouch-item">
								<div class="desc">
                                	<div class="desc-1"><i style="font-weight:bold;">CHENNAI:</i></div>
                                    <div class="desc-3"><i style="font-weight:bold;">
                                  WASH & SAVE ENTERPRISES </i> <br/>
D20, 3rd Street, Ambattur Industrial Estate, <br/> Ambattur, Chennai - 600 058.<br/>  Email: <a href="mailto: Email: info@natecle.in">info@natecle.in </a> <br/></div>

									</div>
                                    <div class="desc-4">
                                    	Ravichandiran      +91 98847 55427 <br/>
                                        Prashant              +91 94455 61308
                                    </div>
                            </div>
							<!--<div class="footer-sosmed">
								<a href="#" title="">
									<div class="item">
										<i class="fa fa-facebook"></i>
									</div>
								</a>
								<a href="#" title="">
									<div class="item">
										<i class="fa fa-twitter"></i>
									</div>
								</a>
								<a href="#" title="">
									<div class="item">
										<i class="fa fa-pinterest"></i>
									</div>
								</a>
								<a href="#" title="">
									<div class="item">
										<i class="fa fa-google"></i>
									</div>
								</a>
								<a href="#" title="">
									<div class="item">
										<i class="fa fa-instagram"></i>
									</div>
								</a>
								<a href="#" title="">
									<div class="item">
										<i class="fa fa-linkedin"></i>
									</div>
								</a>
							</div>-->
						</div>
					</div>
					<!--<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="footer-item">
							<div class="footer-title">
								<h4>RECENT POST</h4>
							</div>
							<div class="footer-blog-item">
								<div class="footer-blog-lead">
									<a href="blog-1.html" title="">How to laundry your suit office - tips and trick.</a>
								</div>
								<div class="footer-blog-date">
									May 29, 2015
								</div>
							</div>
							<div class="footer-blog-item">
								<div class="footer-blog-lead">
									<a href="blog-1.html" title="">How to laundry your suit office - tips and trick.</a>
								</div>
								<div class="footer-blog-date">
									May 29, 2015
								</div>
							</div>

						</div>
					</div>-->
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="footer-item">
                            <div class="footer-getintouch-item">
                                      <div class="desc">
                                        <div class="desc-1"><i style="font-weight:bold;">BANGALORE:</i></div>
                                        <div class="desc-3"><i style="font-weight:bold;">
                                      WASH & SAVE ENTERPRISES </i><br/>
     No.15, Krishnappa Layout, Kerekodi, <br/> Hosakerahalli BSK 3rd Stage, <br/>  Bangalore 560085	.<br/>  Email: <a href="mailto: Email: info@natecle.in">info@natecle.in </a> <br/></div>
                                        </div>
                                <div class="desc-4">
                                              Manjula         +91 76766 53014 <br/>
                                              Satyanarayana   +91 99869 26066
                                        </div> <br/>
                                </div>
                        </div>
					</div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    	<div class="footer-item">
                            <div class="footer-getintouch-item">
                                  <div class="desc">
                                        <div class="desc-1"><i style="font-weight:bold;">PUNE:</i></div>

                                        <div class="desc-3">
                                     <i style="font-weight:bold;">WASH & SAVE ENTERPRISES</i> <br/>
     Gate no – 1, S.no – 40, Chimbli Phata, <br/>   Nasik Road, Kuruli, <br/> Tal-Khed Dist, <br/>Pune – 410501. <br/>  Email: <a href="mailto: Email: info@natecle.in">info@natecle.in </a> <br/></div>
                                        </div>
                                        <div class="desc-4">
                                              Kuppuraj J       +91 98843 20666   <br/>
                                              Gopikrishnan   +91 98419 19191
                                        </div>
                                </div>
                        </div>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="footer-item">
							<div class="footer-title">
								<h4>GET IN TOUCH</h4>
							</div>
							<div class="footer-getintouch">
								<div class="footer-getintouch-item">

									<div class="icon">
										<b class="fa fa-phone"></b>
									</div>
									<div class="desc">
										<div class="desc-1">Phone</div>
										<div class="desc-2">:</div>
										<div class="desc-3">+91 - 44 - 32001617 </div>
									</div>
								</div>
								<div class="footer-getintouch-item">
									<div class="icon">
										<b class="fa fa-envelope "></b>
									</div>
									<div class="desc">
										<div class="desc-1">Email</div>
										<div class="desc-2">:</div>
										<div class="desc-3">
                                        	<a href="mailto:info@natecle.in" title="">info@natecle.in</a><br/>
                                        	<a href="mailto:cse@natecle.in" title="">cse@natecle.in</a><br/>
                                        	<a href="mailto:bd1@natecle.in" title="">bd1@natecle.in</a>
                                        </div>

									</div>
								</div>

								<!--<div class="footer-getintouch-item">
									<div class="icon">
										<b class="fa fa-globe"></b>
									</div>
									<div class="desc">
										<div class="desc-1">Website </div>
										<div class="desc-2">:</div>
										<div class="desc-3">info@natecle.in</div>
									</div>
								</div>-->


							</div>
						</div>

					</div>

				</div>
			</div>

		</div>

		<div class="fcopy">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<p class="ftex">&copy; 2015 Washnsave All Rights Reserved</p>
					</div>
				</div>
			</div>
		</div>

	</div>




	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type='text/javascript' src='https://maps.google.com/maps/api/js?sensor=false&amp;ver=4.1.5'></script>
	<script type='text/javascript' src='js/jqBootstrapValidation.js'></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-hover-dropdown.min.js"></script>
	<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
	<script src="js/bootstrap-image-gallery.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74221211-1', 'auto');

  ga('send', 'pageview');

</script>

</body>


</html>
