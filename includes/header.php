<!DOCTYPE html>
<html class="no-js" lang="en">
  
<body>
	
	<!-- Load page -->
	<div class="animationload">
		<div class="loader"></div>
	</div>
	
	
	<!-- NAVBAR SECTION -->
	<div class="navbar navbar-main navbar-fixed-top">
		<div class="header-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<!--<div class="info">
							<div class="info-item">
								<span class="fa fa-phone"></span> Phone +62 7144 3300
							</div>
							<div class="info-item">
								<span class="fa fa-envelope-o"></span> <a href="#" title="">Email:info@loremipsum.com</a>
							</div>
							
						</div>-->
					</div>
					<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
						<!--<div class="top-sosmed pull-right">
							<a href="#" title=""><span class="fa fa-facebook"></span></a>
							<a href="#" title=""><span class="fa fa-twitter"></span></a>
							<a href="#" title=""><span class="fa fa-instagram"></span></a>
							<a href="#" title=""><span class="fa fa-pinterest"></span></a>
						</div>-->
					</div>
					
				</div>
			</div>
		</div>
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="" /></a>
				
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<!--<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="index.html">Homepage Default</a></li>
						<li><a href="index2.html">Homepage Sliders</a></li>
					  </ul>
					</li>-->
                    <li><a href="index.php">HOME</a></li>
					<li><a href="about.php">ABOUT</a></li>
					<li><a href="brand.php">BRAND</a></li>
					<li><a href="services.php">SERVICES</a></li>
					<li class="dropdown">
					<a class="dropbtn men" href="franchisee.php">FRANCHISEE</a>
					<div class="dropdown-content">
					<a href="existing.php">Existing</a>
    				<a href="upcoming.php">Upcoming</a>
    				</div>
                    </li>
  					
					<!--<li><a href="blog.html">BLOG</a></li>-->
					<li><a href="contact.php">CONTACT US</a></li>
				</ul>
			</div>
		</div>
    </div>