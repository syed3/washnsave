<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>About Wash&Save - Maytag Commercial Laundry Business</title>
    <meta name="description" content="Washnsave  has the intention to satisfy  the customers who  walk in and out of the shop within just one (1) hour. We aim in having fully washed and dried clothes like sarees, shirts, salwars, curtains, bedsheets etc ready within the express delivery time of 1 hour.">
    <meta name="keywords" content="Maytag washer, maytag dryer, laundry business equipment, maytag commercial laundry, dry cleaners, dry cleaning services, dry cleaning service, dry clean service">
    <meta name="author" content="rudhisasmito.com"> 
    <meta name="google-site-verification" content="C3MHHIALf_Db6dTvkO3ClYrhCENS-_LvA6jic0b77P8" />
	
	<!-- ==============================================
	Favicons
	=============================================== -->
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	
	<!-- ==============================================
	CSS
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-image-gallery.min.css" />
	
	
	
	
	
	<!-- ==============================================
	Google Fonts
	=============================================== -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,700,900' rel='stylesheet' type='text/css'>
	
	
	<!-- Custom Stylesheet -->
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	
	
    <script type="text/javascript" src="js/modernizr.min.js"></script>
	
	

</head>

<?php include('includes/header.php'); ?>

 
	<!-- BANNER ROTATOR -->
	<div class="section subbanner" style="background:url('images/slide_page.jpg') no-repeat center center;   -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="caption">
						<h3>About Us</h3>
						<ol class="breadcrumb">
						  <li><a href="index.php">Home</a></li>
						  <li class="active">About Us</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
	
	<!-- ABOUT SECTION -->
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">ABOUT WASH AND SAVE</h2>
						<p class="sublead"></p>
					</div>
				</div>
			</div>
			
			<div class="row">
			
				<div class="col-xs-12 col-md-6">
					<div class="about-img">
						<img src="images/ironbox.png" alt="" class="img-responsive" />
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="about-wrap">
						<h4 class="title-page">OUR HISTORY</h4>
						<p>Wash & Save INDIA presents World class laundry concept to India which is in line with Prime Minister Mr. NarendraModi Government's vision of SwachBharath 2019.</p>
						<p>While the rest of the world is already enjoying this concept, INDIA will move forward. As such, Wash & Save T intends to introduce this concept to the Indian people to be on par with rest of the world.</p>
						<p>A solution to everyone's laundry needs is the principal objective. Our intention is to have the customer in and out of our shop just under one (1) hour, having fully washed and dried their sarees, shirts, salwars, curtains, bedsheets etc.</p>
					</div>
				</div>
				<div class="clearfix"></div>
				
				<!--<div class="why-item-wrap">
					<div class="col-xs-12 col-md-4">
						<div class="why-item">
							<div class="icon">
								<div class="fa fa-paper-plane-o"></div>
							</div>
							<div class="ket">
								<h4>OUR VISION</h4>
								<p>Our templates price really affordable. Only $21 for one beutiful templates, you will get free update and awesome support from us. Still thinking to buy?</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<div class="why-item">
							<div class="icon">
								<div class="fa fa-crosshairs"></div>
							</div>
							<div class="ket">
								<h4>OUR MISSION</h4>
								<p>Of course this is muse templates, you dont need to writing code to edit, you just need to drag and drop your images and change like photoshop. Easy Right!</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<div class="why-item">
							<div class="icon">
								<div class="fa fa-leaf"></div>
							</div>
							<div class="ket">
								<h4>OUR STRENGTHS</h4>
								<p>Easy to edit color, font, icon, or costumizable layout. You can edit with the tool in adobe muse. Everything easy to change and editing.</p>
							</div>
						</div>
					</div>
				
				</div>-->
			</div>
			
		</div>
	</div>
	
	
	
	<!-- STATS SECTION FACTS --> 
	<div class="section stat-facts" style="background:url('images/page_img-u24411-fr.png') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover">
		<!--<div class="bg-overlay">
			<div class="container">
				<div class="row">
					
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="stat-item">
							<div class="icon">
								<i class="fa fa-briefcase"></i>
							</div>
							<div class="stat-title">
								<h3 class="number">450</h3>
								<p>Business Clients</p>
							</div>	
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="stat-item">
							<div class="icon">
								<i class="fa fa-coffee"></i>
							</div>
							<div class="stat-title">
								<h3 class="number">250</h3>
								<p>Cup of Coffee</p>
							</div>	
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="stat-item">
							<div class="icon">
								<i class="fa fa-thumbs-o-up"></i>
							</div>
							<div class="stat-title">
								<h3 class="number">4554</h3>
								<p>People Like Us</p>
							</div>	
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="stat-item">
							<div class="icon">
								<i class="fa fa-users"></i>
							</div>
							<div class="stat-title">
								<h3 class="number">350</h3>
								<p>Employees</p>
							</div>	
						</div>
					</div>
					
				</div>
			</div>
		</div>-->
	</div>
	
	
	<!-- ABOUT SECTION -->
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">THE BRAND TALKS VOLUMES</h2>
						<p class="sublead"></p>
					</div>
				</div>
			</div>
			
			<div class="row pbot-main">
			
				<div class="col-xs-12 col-md-3">
					<img src="images/maytaglogo_trans.png" style="margin-top: 50px" alt="" class="img-responsive" />
					<!--<div class="founder-box">
						<h4 class="title-page">BOKEER DOEL</h4>
						<p>CEO / Founder Company</p>
					</div>-->
				</div>
				<div class="col-xs-12 col-md-9">
					<div class="about-wrap">
						<h4 class="title-page"></h4>
						<p>Maytag USA . For over 100 years, the name has stood for something that’s as important today as it ever was: dependability. As the number one preferred brand in laundry, it’s clear people recognize and value its ongoing devotion. Theroots go back more than a century with the Maytag appliance company, which gave the Maytag USA ® Commercial Laundry brand its launch in 1956. Since then, Maytag USA  has been behind many industry firsts—including the only five-year all-parts warranty on coin-operated equipment—while continually expanding the product portfolio.</p>
						<p>Maytag USA  now offers a full range of user-friendly commercial washers and dryers for the coin, multi-housing, on-premises, and industrial markets. Recognizing the need to help save utility costs and natural resources, energy-efficient solutions are present throughout the lineup.</p>
					</div>
				</div>
				
			</div>
			
		</div>
	</div>
	
	
	
	<!-- STATS SECTION CLIENT --> 
	<!--<div class="section stat-client bg-grey pbot-main" style="">
		<div class="container">
		
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">NATE COMMERCIAL LAUNDRY EQUIPMENT</h2>
						<p class="sublead">WASH & SAVE INDIA is promoted by Nate Commercial laundry equipment Pvt. Ltd., having its headquarter at Ambattur, Chennai,TamilNadu</p>
                        <P>The Promoters of Nate Commercial Laundry have more than 13 years of experience in successfully running Wash & Save laundry operations in MALAYSIA</P>
					</div>
				</div>
			</div>
		
			<div class="row">
				
				<div class="col-xs-12 col-md-2">
					<div class="client-img">
						<img src="images/client12.png" alt="" class="img-responsive" />
					</div>
				</div>
				
				<div class="col-xs-12 col-md-2">
					<div class="client-img">
						<img src="images/client22.png" alt="" class="img-responsive" />
					</div>
				</div>
				
				<div class="col-xs-12 col-md-2">
					<div class="client-img">
						<img src="images/client32.png" alt="" class="img-responsive" />
					</div>
				</div>
				
				<div class="col-xs-12 col-md-2">
					<div class="client-img">
						<img src="images/client42.png" alt="" class="img-responsive" />
					</div>
				</div>
				<div class="col-xs-12 col-md-2">
					<div class="client-img">
						<img src="images/client52.png" alt="" class="img-responsive" />
					</div>
				</div>
				
				<div class="col-xs-12 col-md-2">
					<div class="client-img">
						<img src="images/client62.png" alt="" class="img-responsive" />
					</div>
				</div>
				
				
				
			</div>
		</div>
	</div>-->
	
	
	<!-- FOOTER SECTION -->
<?php include('includes/footer.php'); ?>