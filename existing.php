<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laundry Franchise Business Opportunities for Chennai, Bangalore & Pune</title>
    <meta name="description" content="Wash & Save offers laundry franchise for entrepreneurs looking for  business opportunities in Chennai, Bangloare & Pune. If you think this is an industry which will be a good platform to launch your dream. We would love to hear from you to tell you more. Get in touch with us now!">
    <meta name="keywords" content="laundry business, laundry franchise, laundry business plan, laundry franchise opportunities, laundry franchise in chennai, laundry franchise in bangalore, laundry franchise in pune, laundry franchise in  tamil nadu, franchise laundry business, laundry best franchise, laundry franchise for sale, franchise for laundry business, maytag commercial, laundry franchise, laundry 

business for sale">
    <meta name="author" content="rudhisasmito.com"> 
    <meta name="google-site-verification" content="C3MHHIALf_Db6dTvkO3ClYrhCENS-_LvA6jic0b77P8" />
	
	<!-- ==============================================
	Favicons
	=============================================== -->
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	
	<!-- ==============================================
	CSS
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-image-gallery.min.css" />
	
	
	
	
	
	<!-- ==============================================
	Google Fonts
	=============================================== -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,700,900' rel='stylesheet' type='text/css'>
	
	
	<!-- Custom Stylesheet -->
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	
	
    <script type="text/javascript" src="js/modernizr.min.js"></script>
	
	

</head>
<?php include('includes/header.php'); ?>
	<!-- BANNER -->
	<div class="section subbanner" style="background:url('images/slide_page.jpg') no-repeat center center;   -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="caption">
						<h3>FRANCHISEE</h3>
						<ol class="breadcrumb">
						  <li><a href="index.php">Home</a></li>
						  <li class="active">Franchisee</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
	<!--<div class="section about">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">Why Choose Wash And Save?</h2>
						<p class="sublead"></p>
					</div>
				</div>
			</div>
			
			<div class="row">
			
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-flash"></div>
						</div>
						<div class="ket">
							<h4>OFFICIAL PARTNER WITH MAYTAG</h4>
							<p>Engineered to deliver dependable cleaning results with equally reliable savings.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-money"></div>
						</div>
						<div class="ket">
							<h4>INDIVIDUAL WASHING</h4>
							<p>Hygienic and Safe that ensures no mixing of clothes or communal washing.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-truck"></div>
						</div>
						<div class="ket">
							<h4>Eco-FRIENDLY</h4>
							<p>Energy Saving Schemes such as eco-friendly detergent, waste separation system and energy and water efficient machines.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-life-bouy"></div>
						</div>
						<div class="ket">
							<h4>1 HOUR DELIVERY</h4>
							<p>We strive at completing washing and drying all your clothes within 1 hour, so you don’t have to spend half a day with your dirty clothes.</p>
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
	</div>-->
	<body>
    <div class="section services">
		<div class="container">
        
        	<div class="row">
            	<div class="col-sm-12 col-md-12">
                	<div class="page-title" style="margin-bottom:0px;">
						<h2 class="lead">EXISTING</h2>
                       <div class="page-left"> 
                       <h1 style="font-weight:bold;">CHENNAI</h1>

                        <p>

                        KIP ENTERPRISE<br/>
                        #37/2, Valluvar Kottam High Road,<br/>
                        Jambu Lingam Street,<br/>
                        Nungambakkam,<br/>
                        Chennai- 600 034.<br/>
                        Phone: 9791193734.<br/>
                        </p>
                        <br/>
	                    <br/>
                        <p>
                           <blockquote class="style1"><span style="display:inline-block; width:120px;"></span><span>I was looking to open a Laundromat in Chennai. Being a startup entrepreneur, I had little knowledge about the industry. That's when, I happened to know about Wash and Save Franchisee. The highly experienced <i style="font-weight:bold;">Wash & Save </i> team, went through with me step by step, the workings of the business. Within weeks, the shop was fully set up and successfully operating in Nungambakkam, Chennai.</span></blockquote>
                        </p>

                        <h1><span style="font-weight:bold;">About the franchisee</span></h1><br/>
                        <p><span style="display:inline-block; width:120px;"></span> We, at <i style="font-weight:bold;">Wash & Save</i> (Nungambakkam), take great pride in what we do! The high efficiency washing machines ensure good output and quality of the Laundry. It doesn't stop there.</p>
<p><span style="display:inline-block; width:120px;"></span> 
                        We believe in ecofriendly operations, and be it choosing the detergent, or other chemicals, a lot of research has been done to choose the product, that not only protects your clothes, but also the environment, and that too, at very economical rates!  We avoid distribution of plastics, and even the ones we use are disposed properly for recycling.</p>
<p><span style="display:inline-block; width:120px;"></span> 
                        Laundry doesn't just mean washing and drying clothes. What makes us unique is our commitment towards customer satisfaction.  Our motivated and skilled employees make sure that individual laundry needs of the customer is catered to and delivered on time. So, no more wasting of precious time washing clothes at home.
</p>
    <p>Take your well-earned time-off, and let us do the laundry for you.</p>
                       </div> 
					</div>
               			 
                </div>        
            </div>
            
			
			
			
			
		</div>
	</div>
	<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

</div>
<div id="links" style="margin-left:130px;">
<h1 class="lead"><span style="font-weight:bold;margin-left:12px;">Image Gallery: </span></h1><br/>
<span style="margin:10px;padding:10px;">
    <a href="images/img.gallery1.jpg" title="Shop" data-gallery>
        <img src="images/img_tn1.jpg" alt="shop">
    </a>
    </span>
    <span style="margin:10px;padding:10px;">
    <a href="images/img.gallery2.jpg" title="Shop" data-gallery>
        <img src="images/img_tn2.jpg" alt="shop">
    </a>
    </span>
    <span style="margin:10px;padding:10px;">
    <a href="images/img.gallery3.jpg" title="Shop" data-gallery>
        <img src="images/img_tn3.jpg" alt="shop">
    </a>
    </span>
    <span style="margin:10px;padding:10px;">
     <a href="images/img.gallery4.jpg" title="Shop" data-gallery>
        <img src="images/img_tn4.jpg" alt="shop">
    </a>
    </span>
    <span style="margin:10px;padding:10px;">
    <a href="images/img.gallery5.jpg" title="Shop" data-gallery>
        <img src="images/img_tn5.jpg" alt="shop">
    </a>
    </span>
    <span style="margin:10px;padding:10px;">
    <a href="images/img.gallery6.jpg" title="Shop" data-gallery>
        <img src="images/img_tn6.jpg" alt="shop">
    </a>
    </span>
    <span style="margin:10px;padding:10px;">
     <a href="images/img.gallery7.jpg" title="Shop" data-gallery>
        <img src="images/img_tn7.jpg" alt="shop">
    </a>
    </span>
    
</div>

<br/>
	<br/>
	<br/>
	<br/>
	<br/>
    
    
	</body>
	<!-- FOOTER SECTION -->
<?php include('includes/footer.php'); ?>