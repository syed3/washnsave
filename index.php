<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laundry Service | Clothes Washing & Dry Cleaning Services - WashnSave</title>
    <meta name="description" content="Wash & Save offers you laundry and dry cleaning services in various cities like Chennai, Bangalore & Pune. This Clothes washing & dry cleaning services is one of the best washing services that is hygienic and safe. It ensures no mixing of clothes or communal washing.">
    <meta name="keywords" content="clothes washing service, clothes washing, laundry service, laundry services, dry cleaners, dry cleaning services, dry cleaning service, dry clean service">
    <meta name="author" content="rudhisasmito.com">
    <meta name="google-site-verification" content="C3MHHIALf_Db6dTvkO3ClYrhCENS-_LvA6jic0b77P8" />

	<!-- ==============================================
	Favicons
	=============================================== -->
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">

	<!-- ==============================================
	CSS
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-image-gallery.min.css" />





	<!-- ==============================================
	Google Fonts
	=============================================== -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,700,900' rel='stylesheet' type='text/css'>


	<!-- Custom Stylesheet -->
	<link rel="stylesheet" type="text/css" href="css/style.css" />


    <script type="text/javascript" src="js/modernizr.min.js"></script>



</head>
<?php include('includes/header.php'); ?>


	<!-- BANNER -->

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="top:145px; margin-bottom:60px;">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox" >
    <div class="item active">
    	<div class="carousel-caption">
        <div class="container pos-relative">
			<div class="caption">
				<div class="title-box">
					<h2 style="font-size:30px; font-weight:900; line-height:42px;">Save Money and Time</h2>
				</div>
				<p style="text-shadow:1px 1px 5px #000; color:#000;">A Solution to Everyone’s Laundry Needs</p>
				<a href="services.php" title="" class="btn btn-default">LEARN MORE</a>
			</div>
		</div>
      </div>

     <div class="imgbg" style="background:url(images/editedbanner1.png) no-repeat center center;  -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; min-height:463px; ">

		</div>
    </div>

    <div class="item">
    	<div class="carousel-caption">
        <div class="container pos-relative">
			<div class="caption">
				<div class="title-box">
					<h2 style="font-size:30px; font-weight:900; line-height:42px;">Start your own Laundry</h2>
				</div>
				<p style="text-shadow:1px 1px 5px #000; color:#000;">Partner with Us </p>
				<a href="franchisee.php" title="" class="btn btn-default">LEARN MORE</a>
			</div>
		</div>
      </div>
      <div class="imgbg" style="background:url(images/editedbanner2.png) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;  min-height:463px; ">

		</div>

    </div>

    <div class="item">
    	<div class="carousel-caption">
        <div class="container pos-relative">
			<div class="caption">
				<div class="title-box">
					<h2 style="font-size:30px; font-weight:900; line-height:42px;"> Load in your Worst.</h2>
				</div>
				<p style="text-shadow:1px 1px 5px #000; color:#000;">Maytag USA  will give it the best</p>
                <p style="text-shadow:1px 1px 5px #000; color:#000;">Official Partner – Maytag USA </p>
				<a href="brand.php" title="" class="btn btn-default">LEARN MORE</a>
			</div>
		</div>
      </div>
      <div class="imgbg" style="background:url(images/editedbanner3.png) no-repeat center center;  -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;  min-height:463px;">

		</div>

    </div>

  </div>

  <!-- Controls
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>-->
</div>



	<!---->


	<!-- ABOUT SECTION -->
	<div class="section about" style="padding:40px 0;">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">Why Choose Wash And Save?</h2>
						<p class="sublead"></p>
					</div>
				</div>
			</div>

			<div class="row">

				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-flash"></div>
						</div>
						<div class="ket">
							<h4>OFFICIAL PARTNER WITH MAYTAG USA </h4>
							<p>Engineered to deliver dependable cleaning results with equally reliable savings.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-hand-o-up"></div>
						</div>
						<div class="ket">
							<h4>INDIVIDUAL WASHING</h4>
							<p>Hygienic and Safe that ensures no mixing of clothes or communal washing.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-recycle"></div>
						</div>
						<div class="ket">
							<h4>ECO-FRIENDLY</h4>
							<p>Energy Saving Schemes such as eco-friendly detergent, waste separation system and energy and water efficient machines.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-truck"></div>
						</div>
						<div class="ket">
							<h4>QUICK DELIVERY</h4>
							<p>We strive at completing washing and drying all your clothes within 48 hours, so you don’t have to spend all week with your dirty clothes. We also offer Express Delivery with clean clothes in 1 hour.</p>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>

  <div class="container">
    <div class="row" style="margin-top: 0px;">
      <div class="col-sm-12 col-md-12">
        <div class="page-title" style="margin-top: 0px;">

          <p>Wash & Save provides premium laundry service in partnership with Maytag USA.</p>
         
          <p>Wash & Save believes in providing the highest quality laundry service available.</p>
           
          <p>According to Daily mail, UK "An average mother spends five months of her life doing laundry, says a study. It takes her 26 minutes to go through arduous pre and post-wash tasks every time she turns on the machine, an average of six times a week. On top of that she can expect to spend 55 minutes a week doing the ironing. So before her child hits 18, she will have had 137 days or nearly five months of toil."</p>
           
          <p>Doing laundry is considered as one of the hardest house hold chore by many. So how much time will it take to do the laundry?</p>
           
          <p>This varies on the machine you have and what type of clothes you’re laundering. But, in a typical scenario, a full cycle in the washing machine is 35 minutes and it almost takes 55 minutes to completely dry. A single load from start to finish will likely to take 1.5 hours and then another 15-20 minutes to fold and put away. It can be a two-hour chore for a single load of laundry. Mind you, this doesn't take ironing into account!</p>
           
          <p>Whether you just don’t have the time, ability, or desire to do your own laundry Wash & Save will make sure your laundry receives the utmost care.</p>
           
          <p>Life's too short! Don't spend it doing laundry. Seriously, there are better things to do.</p>
           
          <p>Wash & Save will give your clothes the personal care and attention you won’t find anywhere else.</p>
           
          <p>Give us your dirty laundry, we'll launder it and pack it up for you. Could it become easier?</p>
        </div>
      </div>
    </div>
  </div>


	<!-- SERVICES SECTION -->
	<!--<div class="section services">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">FRANCHISEE PARTNERS</h2>
						<p class="sublead">The Easiest and Cleanest Investment is here for you!.</p>
					</div>
				</div>
			</div>

			<div class="row">

				<div class="col-sm-6 col-md-4">
					<div class="services-item left">
						<div class="icon">
							<img src="images/home-service-img-1.jpg" alt="" class="img-circle" />
						</div>
						<div class="ket">
							<h4></h4>
							<p>Everyone needs Clean Clothes and It’s a necessity in Life.</p>
						</div>
					</div>
					<div class="services-item left">
						<div class="icon">
							<img src="images/home-service-img-2.jpg" alt="" class="img-circle" />
						</div>
						<div class="ket">
							<h4></h4>
							<p>Few or Almost No Employees needed.</p>
						</div>
					</div>
					<div class="services-item left">
						<div class="icon">
							<img src="images/home-service-img-3.jpg" alt="" class="img-circle" />
						</div>
						<div class="ket">
							<h4></h4>
							<p>Provides a Consistent Source of Revenue.</p>
						</div>
					</div>

				</div>

				<div class="col-sm-6 col-md-4 col-md-offset-4">
					<div class="services-item right">
						<div class="icon">
							<img src="images/home-service-img-4.jpg" alt="" class="img-circle" />
						</div>
						<div class="ket">
							<h4></h4>
							<p>Cash Business that pay in advance with little labor required.</p>
						</div>
					</div>
					<div class="services-item right">
						<div class="icon">
							<img src="images/home-service-img-5.jpg" alt="" class="img-circle" />
						</div>
						<div class="ket">
							<h4></h4>
							<p>Recession Proof Business Investment.</p>
						</div>
					</div>
					<div class="services-item right">
						<div class="icon">
							<img src="images/home-service-img-6.jpg" alt="" class="img-circle" />
						</div>
						<div class="ket">
							<h4></h4>
							<p>Enjoy whole Year Business with NO Seasonal Fluctuation.</p>
						</div>
					</div>

				</div>

				<div class="col-sm-12 col-md-4 col-md-offset-4">
					<div class="services-item-image">
						<img src="images/service_img_home2-u24208-fr.png" alt="" />
					</div>
				</div>

			</div>

		</div>
	</div>-->

	<!-- STATS SECTION FACTS -->
	<div class="section stat-facts" style="background:url('images/page_img-u24411-fr.png') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover">
		<!--<div class="bg-overlay">
			<div class="container">
				<div class="row">

					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="stat-item">
							<div class="icon">
								<i class="fa fa-briefcase"></i>
							</div>
							<div class="stat-title">
								<h3 class="number">450</h3>
								<p>Business Clients</p>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="stat-item">
							<div class="icon">
								<i class="fa fa-coffee"></i>
							</div>
							<div class="stat-title">
								<h3 class="number">250</h3>
								<p>Cup of Coffee</p>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="stat-item">
							<div class="icon">
								<i class="fa fa-thumbs-o-up"></i>
							</div>
							<div class="stat-title">
								<h3 class="number">4554</h3>
								<p>People Like Us</p>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="stat-item">
							<div class="icon">
								<i class="fa fa-users"></i>
							</div>
							<div class="stat-title">
								<h3 class="number">350</h3>
								<p>Employees</p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>-->
	</div>


	<!-- TESTIMONIALS SECTION -->
	<!--<div class="section testimonials">
		<div class="container">

			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">TESTIMONIALS</h2>
						<p class="sublead">This template is designed with a unique and simple, so that it can promote and laundry business solution.</p>
					</div>
				</div>
			</div>

			<div class="row">

				<div class="col-sm-12 col-md-6">
					<div class="testimonials-item">
						<div class="people">
							<img src="images/home-testimony-img-1.jpg" alt="" class="img-circle" />
							<h3>John Deol</h3>
							<p>Manager Google</p>
						</div>
						<div class="quote-box">
							<p>"I can't believe the level of help and customer service he provided. I don't think I could have reached my deadline without him."</p>
							<div class="q-url">
								<a href="#" title="">www.google.com</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6">
					<div class="testimonials-item">
						<div class="people">
							<img src="images/home-testimony-img-2.jpg" alt="" class="img-circle" />
							<h3>Sonny Deol</h3>
							<p>Jonitor DC</p>
						</div>
						<div class="quote-box">
							<p>"Hello Rometheme, I just bought a muse template from you. Very happy with it"</p>
							<div class="q-url">
								<a href="#" title="">www.dcentertaiment.com</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6">
					<div class="testimonials-item">
						<div class="people">
							<img src="images/home-testimony-img-3.jpg" alt="" class="img-circle" />
							<h3>Jenny Deol</h3>
							<p>CEO Layer</p>
						</div>
						<div class="quote-box">
							<p>"Hello, I have purchased and used this template. everything is working great!."</p>
							<div class="q-url">
								<a href="#" title="">www.layer.com</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6">
					<div class="testimonials-item">
						<div class="people">
							<img src="images/home-testimony-img-4.jpg" alt="" class="img-circle" />
							<h3>Rambo Deol</h3>
							<p>Dota Player</p>
						</div>
						<div class="quote-box">
							<p>"i thought the designs are flexible to implement, Actually his service are quite good, truly recommended"</p>
							<div class="q-url">
								<a href="#" title="">www.dota.com</a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>-->


	<!-- STATS SECTION CLIENT -->
	<!--<div class="section stat-client p-main" style="">
		<div class="container">
			<div class="row">

				<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
					<div class="client-img">
						<img src="images/client1.png" alt="" class="img-responsive" />
					</div>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
					<div class="client-img">
						<img src="images/client2.png" alt="" class="img-responsive" />
					</div>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
					<div class="client-img">
						<img src="images/client3.png" alt="" class="img-responsive" />
					</div>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
					<div class="client-img">
						<img src="images/client4.png" alt="" class="img-responsive" />
					</div>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
					<div class="client-img">
						<img src="images/client5.png" alt="" class="img-responsive" />
					</div>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
					<div class="client-img">
						<img src="images/client6.png" alt="" class="img-responsive" />
					</div>
				</div>


			</div>
		</div>
	</div>-->


	<!-- BLOG SECTION -->
	<!--<div class="section blog pbot-main">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-6 col-md-offset-3">
					<div class="page-title">
						<h2 class="lead">LATEST NEWS</h2>
						<p class="sublead">This template is designed with a unique and simple, so that it can promote and laundry business solution.</p>
					</div>
				</div>
			</div>

			<div class="row">

				<div class="col-sm-12 col-md-4">
					<div class="blog-item">
						<div class="gambar">
							<div class="icon-news">
								<div class="fa fa-image"></div>
							</div>
							<img src="images/blog-img-1.jpg" alt="" class="img-responsive" />
						</div>
						<div class="item-body">
							<div class="description">
								<p class="lead">
									<a href="blog-1.html" title="">HOW TO LAUNDRY YOUR SUIT OFFICE - TIPS AND TRICK</a>
								</p>
								<p>Yes! Our templates already for desktop, tablet and mobile layout versions. You can use only desktop device or all devices which you need.</p>
							</div>
							<div class="body-footer">
								<div class="author">
									<i class="fa fa-user"></i> BY JOHN DOEL
								</div>
								<div class="date">
									<i class="fa fa-calendar"></i> MAY 29, 2015
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-4">
					<div class="blog-item">
						<div class="gambar">
							<div class="icon-news">
								<div class="fa fa-image"></div>
							</div>
							<img src="images/blog-img-2.jpg" alt="" class="img-responsive" />
						</div>
						<div class="item-body">
							<div class="description">
								<p class="lead">
									<a href="blog-2.html" title="">WELCOME TO OUR OFFICE IN INDONESIA, AUSTIN VISIT US</a>
								</p>
								<p>We give you good documentation to make easy to understand about this templates and the features. if you need we can give video tutorial too.</p>
							</div>
							<div class="body-footer">
								<div class="author">
									<i class="fa fa-user"></i> BY JOHN DOEL
								</div>
								<div class="date">
									<i class="fa fa-calendar"></i> MAY 29, 2015
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-4">
					<div class="blog-item">
						<div class="gambar">
							<div class="icon-news">
								<div class="fa fa-image"></div>
							</div>
							<img src="images/blog-img-1c.jpg" alt="" class="img-responsive" />
						</div>
						<div class="item-body">
							<div class="description">
								<p class="lead">
									<a href="blog-3.html" title="">4 WAYS TO SIMPLY LAUNDRY!(CLEAN MY SPACE)</a>
								</p>
								<p>Create and publish dynamic websites for dekstop and mobile devices that meet the latest web standards without writing any code.</p>
							</div>
							<div class="body-footer">
								<div class="author">
									<i class="fa fa-user"></i> BY JOHN DOEL
								</div>
								<div class="date">
									<i class="fa fa-calendar"></i> MAY 29, 2015
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>-->


	<!-- FOOTER SECTION -->

<?php include('includes/footer.php'); ?>
