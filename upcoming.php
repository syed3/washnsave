<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laundry Business Franchise & Dry Cleaning Franchise – Bangalore & Chennai</title>
    <meta name="description" content="Choose from the best franchise opportunities available to you.  Washnsave is your best solution. Our trained staff will take you through every aspect of opening and operating your laundry efficiently and effectively">
    <meta name="keywords" content="laundry business, laundry franchise, laundry business plan, laundry franchise opportunities, laundry franchise in chennai, laundry franchise in bangalore, laundry franchise in pune, laundry franchise in  tamil nadu, franchise laundry business, laundry best franchise, laundry franchise for sale, franchise for laundry business, maytag commercial, laundry franchise, laundry business for sale">
    <meta name="author" content="rudhisasmito.com"> 
    <meta name="google-site-verification" content="C3MHHIALf_Db6dTvkO3ClYrhCENS-_LvA6jic0b77P8" />
	
	<!-- ==============================================
	Favicons
	=============================================== -->
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	
	<!-- ==============================================
	CSS
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-image-gallery.min.css" />
	
	
	
	
	
	<!-- ==============================================
	Google Fonts
	=============================================== -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,700,900' rel='stylesheet' type='text/css'>
	
	
	<!-- Custom Stylesheet -->
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	
	
    <script type="text/javascript" src="js/modernizr.min.js"></script>
	
	

</head>
<?php include('includes/header.php'); ?>
	<!-- BANNER -->
	<div class="section subbanner" style="background:url('images/slide_page.jpg') no-repeat center center;   -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="caption">
						<h3>FRANCHISEE</h3>
						<ol class="breadcrumb">
						  <li><a href="index.php">Home</a></li>
						  <li class="active">Franchisee</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
	<!--<div class="section about">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
						<h2 class="lead">Why Choose Wash And Save?</h2>
						<p class="sublead"></p>
					</div>
				</div>
			</div>
			
			<div class="row">
			
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-flash"></div>
						</div>
						<div class="ket">
							<h4>OFFICIAL PARTNER WITH MAYTAG</h4>
							<p>Engineered to deliver dependable cleaning results with equally reliable savings.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-money"></div>
						</div>
						<div class="ket">
							<h4>INDIVIDUAL WASHING</h4>
							<p>Hygienic and Safe that ensures no mixing of clothes or communal washing.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-truck"></div>
						</div>
						<div class="ket">
							<h4>Eco-FRIENDLY</h4>
							<p>Energy Saving Schemes such as eco-friendly detergent, waste separation system and energy and water efficient machines.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="why-item">
						<div class="icon">
							<div class="fa fa-life-bouy"></div>
						</div>
						<div class="ket">
							<h4>1 HOUR DELIVERY</h4>
							<p>We strive at completing washing and drying all your clothes within 1 hour, so you don’t have to spend half a day with your dirty clothes.</p>
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
	</div>-->
    <div class="section services">
		<div class="container">
        
        	<div class="row">
            	<div class="col-sm-12 col-md-12">
                	<div class="page-title" style="margin-bottom:0px;">
						<h2 class="lead">UPCOMING</h2>
                       <div class="page-left"> 
                       <h1 style="font-weight:bold;">BANGALORE</h1>

                        <p>
                        INNOVATIVE VENTURES<br/>
                        NO. 88, 1st FLOOR, ABOVE FEDERAL BANK,<br/>
                        CHAMBENAHALLI, SARJAPUR ROAD,
                        BANGALORE – 560035.<br/>
                        Email: info@natecle.in<br/>
                        Phone: 080-22710045 <br/>
                               0897022554 <br/>
                        </p>
                        <br/>
	<br/>
 
                       </div> 
					</div>
               			 
                </div>        
            </div>
            
			
			
			
			
		</div>
	</div>
<br/>
	<br/>
	<br/>
	<br/>
	<br/>
    
    
	
	<!-- FOOTER SECTION -->
<?php include('includes/footer.php'); ?>